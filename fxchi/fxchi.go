package fxchi

import (
	"context"
	"log/slog"
	"net"
	"net/http"

	"github.com/go-chi/chi/v5"
	"go.uber.org/fx"
)

func HttpApp(opts ...fx.Option) fx.Option {
	return fx.Module("fxchi",
		fx.Provide(chi.NewRouter),
		fx.Invoke(fx.Annotate(ServerInvoker, fx.ParamTags(`name:"fxchi"`, `group:"fxchi"`))),
		fx.Options(opts...),
	)
}

type RouteResults struct {
	fx.Out

	Route func(chi.Router) `group:"fxchi"`
}

func NewRoute(fn func(r chi.Router)) func() RouteResults {
	return func() RouteResults {
		return MakeRoute(fn)
	}
}

func MakeRoute(fn func(r chi.Router)) RouteResults {
	return RouteResults{
		Route: fn,
	}
}

func ServerInvoker(srv *http.Server, routes []func(chi.Router), r *chi.Mux, log *slog.Logger, lc fx.Lifecycle) {
	for _, fn := range routes {
		r.Group(fn)
	}
	srv.Handler = r
	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			addr := srv.Addr
			if addr == "" {
				addr = ":http"
			}
			ln, err := net.Listen("tcp", addr)
			if err != nil {
				return err
			}
			go func() {
				log.Info("http server started", "addr", addr)
				srv.Serve(ln)
			}()
			return nil
		},
		OnStop: func(ctx context.Context) error {
			return srv.Shutdown(ctx)
		},
	})
}
