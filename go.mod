module gfx.cafe/util/go

go 1.21.4

toolchain go1.22.0

require (
	github.com/aead/chacha20 v0.0.0-20180709150244-8b13a72661da
	github.com/c2h5oh/datasize v0.0.0-20231215233829-aa82cc1e6500
	github.com/cristalhq/aconfig v0.18.6-0.20231226125657-6e7f9a54f85d
	github.com/go-chi/chi/v5 v5.0.12
	github.com/jackc/pgx/v5 v5.5.5
	github.com/joho/godotenv v1.5.1
	github.com/lmittmann/tint v1.0.4
	github.com/modern-go/reflect2 v1.0.2
	github.com/prometheus/client_golang v1.19.0
	github.com/riverqueue/river v0.5.0
	github.com/riverqueue/river/riverdriver/riverpgxv5 v0.5.0
	github.com/stretchr/testify v1.9.0
	go.uber.org/fx v1.21.0
	golang.org/x/exp v0.0.0-20240318143956-a85f2c67cd81
	golang.org/x/sync v0.7.0
	sigs.k8s.io/yaml v1.4.0
)

require (
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20221227161230-091c0ba34f0a // indirect
	github.com/jackc/puddle/v2 v2.2.1 // indirect
	github.com/lib/pq v1.10.9 // indirect
	github.com/mitchellh/mapstructure v1.5.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/prometheus/client_model v0.5.0 // indirect
	github.com/prometheus/common v0.48.0 // indirect
	github.com/prometheus/procfs v0.12.0 // indirect
	github.com/riverqueue/river/riverdriver v0.5.0 // indirect
	github.com/riverqueue/river/rivertype v0.5.0 // indirect
	go.uber.org/dig v1.17.1 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	go.uber.org/zap v1.27.0 // indirect
	golang.org/x/crypto v0.17.0 // indirect
	golang.org/x/sys v0.18.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	google.golang.org/protobuf v1.32.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
